import itertools
from simpleai.search import (CspProblem, backtrack, min_conflicts,
                             MOST_CONSTRAINED_VARIABLE,
                             LEAST_CONSTRAINING_VALUE,
                             HIGHEST_DEGREE_VARIABLE)
from simpleai.search.csp import _find_conflicts
from datetime import datetime

def AsigAulas():
	
	variables =[
		'Django Girls',
		'Introducción a Python',
		'Keynote Diversidad',
		'Keynote Core Developer',
		'APIs Rest',
		'Sistemas Accesibles',
		'Unit Testing',
		'Editores de Codigo',
		'Musica Python',
		'Vendedor Software',
		'Analisis de Imagenes',
		'Satelites Espaciales',
		'Lib PyPI',
		'Pandas',
	]

	dominios = {
		'Django Girls': [('Laboratorio',10),('Laboratorio',11),('Laboratorio',14),('Laboratorio',15),('Laboratorio',16),('Laboratorio',17)],

		'Introducción a Python' : [('Laboratorio',10), ('Laboratorio',11)],

		'Keynote Diversidad': [('Magna',14),('Magna',15),('Magna',16),('Magna',17)],

		'Keynote Core Developer':[('Magna',14),('Magna',15),('Magna',16),('Magna',17)],

		'APIs Rest':[
						('Magna',10),('Magna',11),('Magna',14),('Magna',15),('Magna',16),('Magna',17),
						('42',10),('42',11),('42',14),('42',15),('42',16),('42',17)
					],

		'Sistemas Accesibles':[
										('Magna',10),('Magna',11),('Magna',14),('Magna',15),('Magna',16),('Magna',17),
										('Laboratorio',10),('Laboratorio',11),('Laboratorio',14),('Laboratorio',15),('Laboratorio',16),('Laboratorio',17)
									],

		'Unit Testing':	[
							('Magna',10),('Magna',11),('Magna',14),('Magna',15),('Magna',16),('Magna',17),
							('42',10),('42',11),('42',14),('42',15),('42',16),('42',17)
						],

		'Editores de Codigo':[('Magna',10),('Magna',11),('Magna',14),('Magna',15),('Magna',16),('Magna',17)],
		
		'Musica Python':[
							('Magna',10),('Magna',11),('Magna',14),('Magna',15),('Magna',16),('Magna',17),
							('42',10),('42',11),('42',14),('42',15),('42',16),('42',17)
						],
		
		'Vendedor Software':[('42',10),('42',11),('Laboratorio',10),('Laboratorio',11)],
		
		'Analisis de Imagenes':[
									('Magna',10),('Magna',11),('Magna',14),('Magna',15),('Magna',16),('Magna',17),
									('42',10),('42',11),('42',14),('42',15),('42',16),('42',17)
								],
		
		'Satelites Espaciales':[('Magna',14),('Magna',15),('Magna',16),('Magna',17),('42',14),('42',15),('42',16),('42',17)],
		
		'Lib PyPI':[
						('Magna',10),('Magna',11),('Magna',14),('Magna',15),('Magna',16),('Magna',17),
						('42',10),('42',11),('42',14),('42',15),('42',16),('42',17)
					],
		
		'Pandas':[('42',10),('42',11),('42',14),('42',15),('42',16),('42',17)]
	}
	
	restricciones = []
	
	def validacion(nom_vars,vals):
		nom_1, nom_2 = nom_vars
		val_1,val_2 = vals

		if nom_1 == 'Keynote Diversidad' or nom_1 == 'Keynote Core Developer' or nom_2 == 'Keynote Diversidad' or nom_2 == 'Keynote Core Developer':
			return val_1[1] != val_2[1]
		
		return (val_1[0] == val_2[0] and val_1[1] != val_2[1]) or val_1[0] != val_2[0]


	# def distinta_hora(vars,vals):
	# 	valor_1, valor_2 = vals
		
	# 	return valor_1[1] != valor_2[1]

	# def distinta_aula(vars,vals):
	# 	valor_1, valor_2 = vals
		
	# 	return (valor_1[0] == valor_2[0] and valor_1[1] != valor_2[1]) or valor_1[0] != valor_2[0]

	# for variable in variables:
	# 	if variable != 'Keynote Diversidad' and variable != 'Keynote Core Developer':
	# 		restricciones.append((('Keynote Diversidad',variable),distinta_hora))
	# 		restricciones.append((('Keynote Core Developer',variable),distinta_hora))
 
	#restricciones.append((('Keynote Core Developer','Keynote Diversidad'),distinta_hora))
	
	for variable_1, variable_2 in itertools.combinations(variables,2):
		restricciones.append(((variable_1,variable_2),validacion))


	return CspProblem(variables, dominios, restricciones)




def resolver(metodo_busqueda, iteraciones):
	problema = AsigAulas()
	
	if metodo_busqueda == "backtrack":
		resultado = backtrack(problema,value_heuristic=HIGHEST_DEGREE_VARIABLE,variable_heuristic=MOST_CONSTRAINED_VARIABLE,inference=False)
	if metodo_busqueda == "min_conflicts":    
		resultado = min_conflicts(problema, iterations_limit=iteraciones)
	
	return resultado





if __name__ == '__main__':
    viewer = None