from simpleai.search import SearchProblem
from simpleai.search.traditional import breadth_first, depth_first, limited_depth_first, iterative_limited_depth_first, uniform_cost, greedy, astar
from simpleai.search.viewers import WebViewer, ConsoleViewer, BaseViewer


INITIAL = (
        (
            (0,2), (0,3), (1,2), (1,3), (2,1), (2,2), (2,3), (3,0), (3,1), (3,2), (4,0), (4,1), (5,0),
          #  (0,2),
          #  (0,3),
          #  (1,2),
          #  (1,3),
          #  (2,1),
          #  (2,2),
          #  (2,3),
          #  (3,0),
          #  (3,1),
          #  (3,2),
          #  (4,0),
          #  (4,1),
          #  (5,0),
        ),
        (
            ((4,4),False),
            ((4,5),False),
            ((5,4),False),
        )
)

POS_MAPA = (2,0)
POS_SALIDA = (5,5)
VOLVER = abs(POS_MAPA[0] - POS_SALIDA[0]) + abs(POS_MAPA[1]-POS_SALIDA[1])

MOVIMIENTOS = [ (1,0),
                (0,1),
                (0,-1),
                (-1,0),
]
DIMENSION = 5

class PlanPiratas(SearchProblem):
    
    def is_goal(self, state):
        _, piratas = state
        for pirata in piratas:
            pos, tiene_mapa = pirata
            if tiene_mapa == True and pos == POS_SALIDA:
                return True
        
        return False

    def actions(self, state):
        _, piratas = state
        piratas = list(piratas)
        acciones = []
        for indice, pirata in enumerate(piratas):
            pos, _ = pirata
            for mov in MOVIMIENTOS:
                pos_nueva = (pos[0] + mov[0] ,pos[1] + mov[1])
                if pos_nueva[0] <= DIMENSION and pos_nueva[1] <= DIMENSION and pos_nueva[0] >= 0 and pos_nueva[1] >= 0:
                    valida =True
                    for p in piratas:
                        if p[0] == pos_nueva:
                            valida = False
                
                    if valida:
                        acciones.append((pos_nueva,indice))
        
        return acciones

    def result(self, state, action):
        franceses, piratas = state
        franceses = list(franceses)
        piratas = list(piratas)
        pos_nueva, indice = action
        
        if pos_nueva in franceses:
            franceses.remove(pos_nueva)
            piratas.pop(indice)
        else:
            _, tiene_mapa = piratas[indice]
            if pos_nueva == POS_MAPA:
                tiene_mapa = True

            piratas[indice] = pos_nueva,tiene_mapa
                
        return tuple(franceses), tuple(piratas)

    def cost(self, state1, action, state2):
        return 1

    def heuristic(self, state):
        _, piratas = state
        minimo = VOLVER * 2
        for pirata in piratas:
            (pos_x,pos_y), tiene_mapa = pirata
            distancia_pirata =0
            if tiene_mapa:
                distancia_pirata = abs(pos_x - POS_SALIDA[0]) + abs(pos_y - POS_SALIDA[1])
            else:
                distancia_pirata = abs(pos_x - POS_MAPA[0]) + abs(pos_y - POS_MAPA[1]) + VOLVER
            
            if  distancia_pirata < minimo:
                minimo = distancia_pirata
        
        return minimo

def resolver(metodo_busqueda, franceses, piratas):
    lista_piratas = []
    #viewer = BaseViewer()
    viewer = None

    for pirata in piratas:
       lista_piratas.append((pirata,False))

    estado_inicial = (tuple(franceses),tuple(lista_piratas))

    problema = PlanPiratas(estado_inicial)

    if metodo_busqueda == "breadth_first":    
       resultado = breadth_first(problema, graph_search=True, viewer = viewer)
    
    if metodo_busqueda == "greedy":    
       resultado = greedy(problema, graph_search=True, viewer = viewer)
    
    if metodo_busqueda == "depth_first":    
       resultado = depth_first(problema, graph_search=True, viewer = viewer)
    
    if metodo_busqueda == "astar":
       resultado = astar(problema, graph_search=True, viewer = viewer)
    
    if metodo_busqueda == "uniform_cost":
       resultado = uniform_cost(problema, graph_search=True, viewer = viewer)
    
    """
        print(viewer.stats)
        print("Profundidad solucion: {}".format(len(resultado.path())))
        print("Costo: {}".format(resultado.cost))
    """
    
    return resultado



if __name__ == '__main__':
	"""
    #viewer = WebViewer()
	#viewer = ConsoleViewer()
	viewer = None
	#viewer = BaseViewer()

	#metodo = "depth_first"
    #metodo = "breadth_first"
	#metodo = "astar"
	#metodo = "uniform_cost"
	metodo = "greedy"

	franceses = [(0,2), (0,3), (1,2), (1,3), (2,1), (2,2), (2,3), (3,0), (3,1), (3,2), (4,0), (4,1), (5,0)]
	piratas = [(4,4), (4,5), (5,4)]

	result = resolver(metodo, franceses, piratas)

	print("#" * 80)
	print("Franceses:")
	print(franceses)
	print("Piratas:")
	print(piratas)
	print("#" * 80)
	print("Estado final: {}".format(result.state))
	print("#" * 80)
	for accion, resultado in enumerate(result.path()):
		print('Accion',accion, ':', resultado[0])
		print('Resultado:', resultado[1])

	# print("#" * 80)
	# print("Nodos Visitados: {}".format(viewer.stats['visited_nodes']))
	# print("Profundidad solucion: {}".format(len(result.path())))
	# print("Costo: {}".format(result.cost))
	# print("Tamaño máximo frontera: {}".format(viewer.stats['max_fringe_size']))
	"""